// [Section] Comparison Query Operators

// $gt/$gte operator
	/*
		- It allows us to find document hat have field number value greater than or equal to a specified value
		- Syntax:
		db.collectionName.find({field: {$gt: value}});
		db.collectionName.find({field: {$gte: value}});
	*/

	// $gt
	db.users.find({age:{$gt: 50}}).pretty();

	// $gte
	db.users.find({age: {$gte: 21}}).pretty();

// $lt / $lte operator
	/*
		- It allows us to find/retrieve documents that have field number values less than or equal to a specified value
		- Syntax:
		db.collectionName.find({field: {$lt: value}});
		db.collectionName.field({field: {$lte: value}});
	*/

	db.users.find({age: {$lt: 50}});
	db.users.find({age: {$lte: 76}});

// $ne operator
	/*
		- It allows us to find documents that have field number values not equal to a specified value
		- Syntax:
			db.collectionName.find({field: {$ne: value}});
	*/

	db.users.find({age: {$ne: 68}});

// $in operator
	/*
		- It allows us to find document(s) with specific match criteria one field using different values.
		- Syntax:
			db.collectionName.find({field: {$in: [valueA, valueB]}});
	*/

	db.users.find({lastName: {$in: ["Doe", "Hawking"]}})

	db.users.find({courses: {$in: ["HTML", "React"]}});

	db.users.find({courses: {$in: ["CSS", "JavaScript"]}});

// [Section] Logical Query Operators
	// $or operator
	/*
		- allows us to find document that match a single criteria from multiple provided search criteria.
		db.collectionName.find({$or: [{fieldA: valueA}, {fieldB: valueB}]})
	*/

	db.users.find({$or: [{firstName: "Neil"}, {age: 25}]});

	db.users.find({$or: [{firstName: "Neil"}, {age: {$gt: 30}}]});

	// $and operator
	/*
		- It allows us to find documents matching all provided criteria.
		- Syntax:
			db.collectionName({$and: [{fieldA: valueA}, {fieldB: valueB}]});
	*/

	db.users.find({$and: [{firstName: "Neil"}, {age: {$gt: 30}}]});

	db.users.find({$and: [{firstName: "Neil"}, {age: {$gt: 25}}]});

// [Section] Field Projection
	/*
		- Retrieving documents are common operations that we do and by default mongoDB query return the whole document as a response
	*/

	// inclusion
	/*
		- It allows us to include/add specific fields only when retrieving documents
		- Syntax:
			db.collectionName.find({criteri}, {fields: 1});
	*/

	db.users.find({firstName: "Jane"},
		{
			firstName: 1,
			lastName: 1,
			age: 1,
		})

	db.users.find({$and: [{firstName: "Neil"}, {age: {$gt: 30}}]},
		{
			firstName: 1
		})

	// Exclusion
		/*
			- Allows us to exclude/remove specific fields only when retrieving documents.
			- The value provided is 0
		*/

		db.users.find({firstName: "Jane"}, {
			contact:0,
			department: 0
		})

		db.users.find({$and: [{firstName: "Neil"}, {age: {$gte: 25}}]},
		{
			age: 0,
			courses: 0
		})

	// Returning Specific field in Embedded documents

	db.users.find({firstName: "Jane"}, {
		"contact.phone": 1,
	})

	db.users.find({firstName: "Jane"}, {
		"contact.phone": 0,
	})

// [Section] Evaluation Query Operators
	/*
		- It allows us to find documents that match specific string pattern using regular expression
		Syntax:
			db.users.find({field: $regex : "pattern", $options: "$optionValue"})
	*/

	// case sensitive query
	db.users.find({firstName: {$regex: "Ne"}});
	// case insensitive query
	db.users.find({firstName: {$regex: "Ne", $options: "i"}});

	// CRUD operations
	db.users.updateOne({age: {$lte: 17}},{
		$set:{
			firstName: "Chris",
			lastName: "Mortel"
		}
	})

	db.users.deleteOne({$and:[{firstName: "Chris"}, {lastName: "Mortel"}]});
	